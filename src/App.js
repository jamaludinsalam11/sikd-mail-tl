import React,{useEffect, useState} from 'react'
import {BrowserRouter as Router} from 'react-router-dom';
import { StylesProvider, createGenerateClassName } from '@material-ui/core/styles'
import axios from 'axios'
import MailTL from './components/MailTL'
const generateClassName = createGenerateClassName({
    productionPrefix: 'mail-tl',
  });
export default function App(props){
    return(
        <Router>
            <StylesProvider generateClassName={generateClassName}>
                <MailTL/>
            </StylesProvider>
        </Router>
        
    )
}