require('file-loader?name=[name].[ext]!./index.html');
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import dotenv from 'dotenv'

const appElement = document.getElementById('app-mail-tl');

ReactDOM.render(<App/>, appElement)

// dotenv.config()